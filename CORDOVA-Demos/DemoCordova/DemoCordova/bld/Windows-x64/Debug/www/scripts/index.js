﻿// Si quiere una introducción sobre la plantilla En blanco, vea la siguiente documentación:
// http://go.microsoft.com/fwlink/?LinkID=397704
// Para depurar código al cargar la página en dispositivos/emuladores Ripple o Android: inicie la aplicación, establezca puntos de interrupción 
// y ejecute "window.location.reload()" en la Consola de JavaScript.
(function (angular) {
    "use strict";
    // Este evento se dispara cuando el dispositivo responde por primera vez
    document.addEventListener( 'deviceready', onDeviceReady.bind( this ), false );

    // definimos las variables de trabajo
    var loc = angular.module("locFinder", []);
    loc.controller("locFinderController", [$scope, function ($scope) {
        $scope.lat = 0;
        $scope.long = 0;

        this.getLocation = function getLocation() {
            navigator.geolocation.getCurrentPosition(this.onLocationFound)
        };
        this.onLocationFound = function onLocationFound(position) {
            $scope.$apply(function () {
                $scope.lat = position.coords.latitude;
                $scope.long = position.coords.longitude;
            })
        };
    }]);

    function onDeviceReady() {
        // Controlar la pausa de Cordova y reanudar eventos
        document.addEventListener( 'pause', onPause.bind( this ), false );
        document.addEventListener( 'resume', onResume.bind( this ), false );
        
        // TODO: Cordova se ha cargado. Haga aquí las inicializaciones que necesiten Cordova.
    };

    function onPause() {
        // TODO: esta aplicación se ha suspendido. Guarde el estado de la aplicación aquí.
    };

    function onResume() {
        // TODO: esta aplicación se ha reactivado. Restaure el estado de la aplicación aquí.
    };
    // En la llamada inicial, le pasamos el objeto Angular para que esté 
    // disponible desde el inicio
} )(window.angular);