﻿// Para depurar código al cargar la página en dispositivos/emuladores Ripple o Android: 
// iniciar la aplicación, establecer puntos de interrupción 
// y ejecutar "window.location.reload()" en la Consola de JavaScript.
(function () {
    "use strict";

    document.addEventListener( 'deviceready', onDeviceReady.bind( this ), false );

    function onDeviceReady() {
        // Controlar la pausa de Cordova y reanudar eventos
        document.addEventListener( 'pause', onPause.bind( this ), false );
        document.addEventListener( 'resume', onResume.bind( this ), false );
        
        // Invocamos el proceso de llamada a geolocalización
        navigator.geolocation.getCurrentPosition(posicionDisponible, onError);
    };

    // Posición leída 
    function posicionDisponible(position) {
        var element = document.getElementById('geolocation');
        element.innerHTML = 'Latitud: ' + position.coords.latitude + '<br />' +
                            'Longitud: ' + position.coords.longitude + '<br />' +
                            'Altitud: ' + position.coords.altitude + '<br />' +
                            'Precisión: ' + position.coords.accuracy + '<br />' +
                            'Precisión de Altitud: ' + position.coords.altitudeAccuracy + '<br />' +
                            'Dirección: ' + position.coords.heading + '<br />' +
                            'Velocidad: ' + position.coords.speed + '<br />' +
                            'Timestamp: ' + position.timestamp + '<br />';
    }

    // Callback para errores: recibe el objeto PositionError 
    //
    function onError(error) {
        alert('Código: ' + error.code + '\n' +
                'Mensaje: ' + error.message + '\n');
    }


    function onPause() {
        // TODO: esta aplicación se ha suspendido. Guarde el estado de la aplicación aquí.
    };

    function onResume() {
        // TODO: esta aplicación se ha reactivado. Restaure el estado de la aplicación aquí.
    };
} )();